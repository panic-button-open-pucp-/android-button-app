package com.pucp.open.android.panico;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private final String TAG = "[FIREBASE]";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        // From where it has been sent
        // String from = remoteMessage.getFrom();
        // Log.d(TAG, "Message received from(getFrom): " + from);

        // Body of the notification received
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Notification(getBody): " + remoteMessage.getNotification().getBody());
            showNotification("NOTIFICATION title", "NOTIFICATION text");
        }

        // Data received from the notification
        if (remoteMessage.getData().size() > 0) {
            // Log.d(TAG, "Data(getData): " + remoteMessage.getData());
            // TODO: Process json data
            showNotification("EMERGENCIA EN OPEN PUCP", "Se ha presionado un botón de pánico");
        }
    }

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        // Log.d(TAG, "Refreshed token: " + token);
        SharedPreferences preferences = getSharedPreferences(UserPreferences.PREFERENCES, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(UserPreferences.VAR_USER_TOKEN, token);
        editor.apply();

        // CONECTION TO ARTURO ACCOUNT

    }

    public static String getToken(Context context) {
        return context.getSharedPreferences(UserPreferences.PREFERENCES, MODE_PRIVATE)
                .getString(UserPreferences.VAR_USER_TOKEN, UserPreferences.VAR_DEFAULT);
    }

    private void showNotification(String title, String message) {
        long[] vibrate = {0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300};
        // int colorPrimary = getColor(R.color.colorPrimary);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContentTitle(title)
                .setContentText(message)
                .setVibrate(vibrate)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        // NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, builder.build());
    }
}
