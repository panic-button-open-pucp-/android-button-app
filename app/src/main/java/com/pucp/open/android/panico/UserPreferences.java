package com.pucp.open.android.panico;

public final class UserPreferences {

    // preference name
    public static final String PREFERENCES = "USER_DATA_PREF";

    // variables
    public static final String VAR_FIRST_TIME = "isFirstTime";
    public static final String VAR_USER_NAME = "userName";
    public static final String VAR_USER_TOKEN = "userToken";

    public static final String VAR_DEFAULT = "None";
}
