package com.pucp.open.android.panico;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.pucp.open.android.panico.api.OpenPucpApiService;

import java.io.IOException;

public class PasswordActivity extends AppCompatActivity {

    private EditText txt_password;
    private Button btn_send;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);

        txt_password = findViewById(R.id.txt_password);
        btn_send = findViewById(R.id.btn_send_to_api);

        btn_send.setOnClickListener(this::sendToApi);
    }

    public void sendToApi(View view) {
        String password = txt_password.getText().toString();

        // TODO: Do something with password variable, like IMPLEMENT security authentication
        if (!password.equals("")) {
            SharedPreferences preferences = getSharedPreferences(UserPreferences.PREFERENCES, Context.MODE_PRIVATE);

            String username = preferences.getString(UserPreferences.VAR_USER_NAME, UserPreferences.VAR_DEFAULT);
            String token = MyFirebaseMessagingService.getToken(this);

            try {
                OpenPucpApiService.postSendGuardData(username, token, this);

            } catch (IOException e) {
                Toast.makeText(this, "No se pudo registrar este móvil, inténtelo más tarde", Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }

        } else {
            Toast.makeText(this, "password único necesario", Toast.LENGTH_SHORT).show();
        }
    }
}
