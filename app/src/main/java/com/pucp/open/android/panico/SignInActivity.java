package com.pucp.open.android.panico;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class SignInActivity extends AppCompatActivity {

    private EditText txt_guardName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        txt_guardName = findViewById(R.id.txt_name);
    }


    public void guard_name(View view) {
        // just if user has put his name in EditText Field
        String name = txt_guardName.getText().toString();

        if (!name.equals("")) {
            SharedPreferences preferences = getSharedPreferences(UserPreferences.PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();

            // save user name in Shared Preferences
            editor.putString(UserPreferences.VAR_USER_NAME, name);
            editor.apply();

            // Show the last activity to put a password
            Intent intent = new Intent(SignInActivity.this, PasswordActivity.class);
            finish();
            startActivity(intent);

        } else {
            Toast.makeText(this, "Nombre requerido", Toast.LENGTH_LONG).show();
        }
    }
}
