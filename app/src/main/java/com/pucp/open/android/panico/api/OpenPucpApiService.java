package com.pucp.open.android.panico.api;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.pucp.open.android.panico.MainActivity;
import com.pucp.open.android.panico.UserPreferences;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class OpenPucpApiService {

    private static final MediaType JSON = MediaType.get("application/json; charset=utf-8");
    private static final String URL = "https://open-pucp-panic-button.herokuapp.com/api/";
    private static OkHttpClient client = new OkHttpClient();

    public static void postSendGuardData(String username, String token, Context context) throws IOException{

        String json = (new Guard(username, token)).toJson();

        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder().url(URL+"guards").post(body).build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code: " + response);
                } else {
                    processResponse(response.body().string(), context);
                }
            }
        });
    }


    private static class Guard {
        private String email;
        private String token;

        Guard(String email, String token) {
            this.email = email;
            this.token = token;
        }

        String toJson() {
            Gson gson = new Gson();
            return gson.toJson(this);
        }
    }

    private static void processResponse(String message, Context context){
        // Toast.makeText(context, message, Toast.LENGTH_LONG).show();

        // change state of first time launch variable
        SharedPreferences preferences = context.getSharedPreferences(UserPreferences.PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(UserPreferences.VAR_FIRST_TIME, false);
        editor.apply();

        // launch principal activity
        Intent intent = new Intent(context, MainActivity.class);
        ((Activity)context).finish();
        context.startActivity(intent);
    }
}
