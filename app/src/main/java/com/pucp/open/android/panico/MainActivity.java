package com.pucp.open.android.panico;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // by default, is true, after first launch, it's false
        SharedPreferences preferences = getSharedPreferences(UserPreferences.PREFERENCES, Context.MODE_PRIVATE);
        boolean isFirstTime = preferences.getBoolean(UserPreferences.VAR_FIRST_TIME, true);

        // launch to get guard data as user
        if (isFirstTime) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(UserPreferences.VAR_FIRST_TIME, false);

            // commit to change the boolean value of first time launch
            editor.apply();

            // launch SingIn Activity
            Intent intent = new Intent(this, SignInActivity.class);
            finish();
            startActivity(intent);
        }
    }
}
